from page_objects import PageObject, PageElement

class FramesPage(PageObject):
    def check_gage(self):
        return "Frame Page" in self.w.title
